# yourbot

Deploy project for yourbot

## Deploy steps


// Up docker-compose 

`docker-compose -f docker-compose-linux.yml up -d`

// Copy seed data to mongo container

`docker cp backup_bots_data/ yourbot-v1_mongodb_1:/backup_bots_data`

// Restore as seed data for mongo 

`docker exec -it yourbot-v1_mongodb_1 mongorestore backup_bots_data/dump`

// Restart yourbot container for new data

`docker-compose -f docker-compose-linux.yml restart yourbot`